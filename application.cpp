#include "application.h"
#include "gui.h"
#include "gui/constants.h"

#include <QObject>
#include <QTimer>
#include <QSerialPortInfo>
#include <QStringList>
#include <QDebug>
#include <QString>
#include <QList>

Application::Application(int argc, char *argv[]) :
    qApplication(argc, argv),
    gui(*this)
{
    QStringList baudrateList;
    for(const auto &it: QSerialPortInfo::standardBaudRates()){
        baudrateList.push_back(QString::number(it));
    }
    gui.setBaudrateList(baudrateList, QStringLiteral("115200"));
    gui.setDataBitsList(gui::dataBitsList, QStringLiteral("8"));
    gui.setStopBitsList(gui::stopBitsList, QStringLiteral("1"));
    gui.setParityList(gui::parityList, QStringLiteral("No parity"));
    gui.setFlowControlList(gui::flowControlList, QStringLiteral("Off"));
    refreshPorts();
    qConnect();
}

int Application::run()
{
    devicePollTimer.start(500);
    return QApplication::exec();
}

void Application::openConnection(bool open)
{
    if(activeSerialPort.isOpen()){
        activeSerialPort.close();
    }
    if(open){
        activeSerialPort.open(QIODevice::ReadWrite);
    }
}

void Application::qConnect()
{
    connect(&devicePollTimer, &QTimer::timeout, this, &Application::refreshPorts);
}

void Application::refreshPorts(){
    QStringList currentPortList = gui.getSerialPortList();
    QList<QSerialPortInfo> newList = QSerialPortInfo::availablePorts();
    if(newList.count() != currentPortList.count()){
        QStringList updatedList;
        for(const auto &newPort: newList){
            updatedList.push_back(newPort.portName() + " " + newPort.description());
        }
        QString currentPort = gui.getCurrentSerialPort();
        gui.setSerialPortList(updatedList, currentPort);
    }
}

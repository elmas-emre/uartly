QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    application.cpp \
    gui.cpp \
    main.cpp \
    gui/inputoutputview.cpp \
    gui/inputsettingsview.cpp \
    gui/sendview.cpp \
    gui/serialsettingsview.cpp

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    application.h \
    gui.h \
    gui/constants.h \
    gui/inputoutputview.h \
    gui/inputsettingsview.h \
    gui/sendview.h \
    gui/serialsettingsview.h
